import getProducts from "@/actions/Products/action-get-products";
import Product from "../Product";
import { ProductProps } from "../Product";


interface ProductsProps {
  products: ProductProps[];
}

const Products = async (search: any) => {
  const { products } = await getProducts(search);

  return (
    <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
      {products?.map((product: ProductProps) => (
        <Product key={product.id} {...product} />
      ))}
    </div>
  );
};

export default Products;