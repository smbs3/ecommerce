"use client";
import { onFormPostAction } from "@/actions/order/action-create";
import { useRouter } from 'next/navigation'
import { useFormState } from "react-dom";


type PayProps = {

    cardId: string;
    total: number;
};

const Pay: React.FC<PayProps> = ({total, cardId}) => {
    const router = useRouter()
    
    const [state, action]=useFormState(onFormPostAction,{
        message:"",
    })

    if (state.message=="success"){
        router.push("/success")
    }
    
    return (
        <div className="container mx-auto mt-10">
        <form action={action} className="max-w-xl mx-auto bg-white p-8 text-black rounded shadow-lg">
            <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
                <div>
                    <h3 className="text-lg font-semibold text-red-600 mb-4">Dirección de Facturación</h3>
                    <div className="mb-4">
                        <label htmlFor="name" className="block text-sm font-medium text-gray-700">Nombre Completo:</label>
                        <input type="text" id="name" placeholder="Ingresa tu nombre completo" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="address" className="block text-sm font-medium text-gray-700">Dirección:</label>
                        <input type="text" name="shipping_address" id="shipping_address" placeholder="Ingresa tu dirección" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                    </div>
                </div>
    
                <div>
                    <h3 className="text-lg font-semibold text-red-600 mb-4">Pago</h3>
                    <div className="mb-4">
                        <label htmlFor="cardName" className="block text-sm font-medium text-gray-700">Nombre en la Tarjeta:</label>
                        <input type="text" id="cardName" placeholder="Ingresa el nombre en la tarjeta" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                    </div>
                    <div className="mb-4">
                        <label htmlFor="cardNumber" className="block text-sm font-medium text-gray-700">Número de Tarjeta:</label>
                        <input type="text" id="cardNumber" placeholder="Ingresa el número de tarjeta" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                    </div>
                    <div className="flex justify-between mb-4">
                        <div className="w-1/2 mr-2">
                            <label htmlFor="expMonth" className="block text-sm font-medium text-gray-700">Mes de Expiración:</label>
                            <input type="text" id="expMonth" placeholder="MM" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                        </div>
                        <div className="w-1/2 ml-2">
                            <label htmlFor="expYear" className="block text-sm font-medium text-gray-700">Año de Expiración:</label>
                            <input type="text" id="expYear" placeholder="YYYY" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                        </div>
                    </div>
                    <div className="mb-4">
                        <label htmlFor="cvv" className="block text-sm font-medium text-gray-700">CVV:</label>
                        <input type="text" id="cvv" placeholder="Ingresa el CVV" required className="mt-1 block w-full border-gray-300 rounded-md shadow-sm focus:ring-red-500 focus:border-red-500" />
                    </div>
                </div>
            </div>
            <input type="hidden" name="cart_id" value={cardId} id="card_id" />
            <input type="hidden" name="total" value={total} id="total" />

            <input type="submit" value="Proceder al Pago" id="checkoutBtn" className="mt-6 px-4 py-2 bg-red-600 text-white rounded-md hover:bg-red-700" />
        </form>
    </div>
    
    );
};

export default Pay;