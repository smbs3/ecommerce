import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

interface detailsOrder {
    id: number,
    user_id: string,
    total: number,
    order_status: string
}


const CardOrder: React.FC<detailsOrder> = ({ id, user_id, total, order_status }) => {
    return (

        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
            <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                {id}
            </th>
            <td className="px-6 py-4">
               {order_status}
            </td>
            <td className="px-6 py-4">
            $ {total}
            </td>
            <td className="px-6 py-4">
                <Link href={`/order/${id}`}> View Order</Link>
            </td>
        </tr>

    )
}

export default CardOrder


