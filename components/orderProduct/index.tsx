import Image from 'next/image'
import React from 'react'

interface ProductI {
    id:number;
    name:string;
    price:number;
    image_url:string;
    description:string;
}

interface OrderProductProps {
    id:number;
    quantity:number;
    products: ProductI;
}

const OrderProduct = (props: OrderProductProps) => {

    const {products}=props;
    const {id, name, price, image_url, description} =products;

    return (
        <div className="flex flex-col lg:flex-row items-center py-6 border-b border-gray-200 gap-6 w-full">
            <div className="img-box max-lg:w-full">
                <Image src={image_url} width={400} height={400} alt="Premium Watch image" className="aspect-square w-full lg:max-w-[140px]"/>
            </div>
            <div className="flex flex-row items-center w-full ">
                <div className="grid grid-cols-1 lg:grid-cols-2 w-full">
                    <div className="flex items-center">
                        <div className="">
                            <h2 className="font-semibold text-xl leading-8 text-black mb-3">
                                {name}</h2>
                                <p className='font-medium text-base leading-7 text-black'>{description}</p>
                            <div className="flex items-center ">
                                <p className="font-medium text-base leading-7 text-black ">Qty: <span className="text-gray-500">2</span></p>
                            </div>
                        </div>

                    </div>
                    <div className="grid grid-cols-12">
                        <div className="col-span-5 lg:col-span-1 flex items-rigth max-lg:mt-3">
                            <div className="flex gap-3 lg:block">
                                <p className="font-medium text-sm leading-7 text-black">price</p>
                                <p className="lg:mt-4 font-medium text-sm leading-7 text-indigo-600">${price}</p>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    )
}

export default OrderProduct