import Image from "next/image";
import AddToCartButton from "../addCard";


export interface ProductProps {
  id: number;
  name: string;
  image_url: string;
  description: string;
  price: number;
  available: boolean;
  imageUrl?: string;
}

const Product: React.FC<ProductProps> = ({ id, name, description, price, available, image_url }) => {
  return (
    <div className="border p-4 rounded-lg shadow-lg">
      <Image
        src={image_url || '/placeholder.png'}
        width={500}
        height={500}
        alt={name}
        className="w-full h-64 object-cover object-center rounded-t-lg"
      />

      <div className="p-4">
        <h3 className="text-lg font-semibold">{name}</h3>
        <p className="text-gray-700">{description}</p>
        <p className="text-gray-900">${price}</p>
        {available ? (
          <AddToCartButton productId={id} text="Add cart" />
        ) : (
          <p className="text-red-500">No disponible</p>
        )}
      </div>
    </div>
  );
};

export default Product;
