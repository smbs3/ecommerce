"use client";
import { useState, useEffect } from "react";
import { useSearchParams, usePathname, useRouter } from "next/navigation";
import useDebounce from "@/hooks/useDebounce";

const Search = () => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const { replace } = useRouter();
  const [searchTerm, setSearchTerm] = useState<string>(
    searchParams.get("search") ?? ""
  );
  const debouncedSearchTerm = useDebounce({ value: searchTerm, delay: 1000 });

  useEffect(() => {
    const params = new URLSearchParams(searchParams.toString());

    if (debouncedSearchTerm) {
      params.set("search", debouncedSearchTerm);
    } else {
      params.delete("search");
    }
    replace(`${pathname}?${params.toString()}`, undefined);
  }, [debouncedSearchTerm, searchParams]);

  return (
    <div className="relative flex flex-1 flex-shrink-0 mt-5">
      <label htmlFor="search" className="sr-only">
        Search
      </label>
      <input
        type="text"
        id="simple-search"
        className="bg-gray-50 border mb-5 border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 h-16 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        placeholder="Search..."
        required
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
    </div>
  );
};

export default Search;