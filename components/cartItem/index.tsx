
interface ProductCartItem {
  name: string;
  image_url: string;
  price: number;
  description: string;
}

interface CartItemProps {
  id: number;
  cart_id: number;
  product_id: number;
  quantity: number;
  products: ProductCartItem;
}

const CartItem: React.FC<CartItemProps> = (props: CartItemProps) => {
  const { products, quantity } = props;

  const { name, price, image_url, description } = products;
  return (

    <div className="flex items-center gap-4">
      <img src={image_url} alt={name} className="w-20 h-20 object-cover rounded" />
      <div>
        <h2 className="font-semibold">{name}</h2>
        <p>{description}</p>
        <p>Cantidad: {quantity}</p>
        <p>Precio unitario: ${price}</p>
        <p>Subtotal: ${price * quantity}</p>
      </div>
    </div>


  );
};

export default CartItem;