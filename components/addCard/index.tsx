'use client'
import addToCart from '@/actions/Cart/action-cart';

interface AddToCartButtonProps {
  productId: number;
  text:string;
}

const AddToCartButton: React.FC<AddToCartButtonProps> = ({ productId , text}) => {
//   const addToCarts  = addToCart();

  const handleAddToCart = (productId: number) => {
    const addCarts=addToCart(productId, 1);
  };

  return (
      <button onClick={() => handleAddToCart(productId)} className='mt-4 px-4 py-2 rounded bg-blue-500  bg-gray-500 text-white'>
       {text}
      </button>
  );
};

export default AddToCartButton;
