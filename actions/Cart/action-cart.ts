import { AxiosRestClient } from "@/utils/axiosConfig";
import { createClient } from "@/utils/supabase/client";


const addToCart = async (productId: number, quantity: number) => {
  try {
    const supabase = createClient()
    const {data: {user}}  = await supabase.auth.getUser();
    const axiosClient = AxiosRestClient(user?.id);

    const response = await axiosClient.post(`/cart`, {
      productId,
      quantity
    });

    if (response.status === 200) {
      return { success: true };
    } else {
      throw new Error("Failed to add product to cart");
    }
  } catch (error) {
    return { error: 'Failed to add product to cart.' };
  }
};

export default addToCart;
