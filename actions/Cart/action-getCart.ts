
import { createClient } from "@/utils/supabase/server";
import { AxiosRestClient } from "@/utils/axiosConfig";



const getCarts = async () => {
  try {
    const supabase = createClient()
    const {data: {user}}  = await supabase.auth.getUser();
    const axiosClient = AxiosRestClient(user?.id);

    const response = await axiosClient.get(`/cartdetails`);
    if (response.status === 200) {
      const cart = response.data;
      return {
        cart:cart.cart
      };
    } else {
      throw new Error('cart not found');
    }
  } catch (error) {
    return { cart:[] };
  }
};

export default getCarts;