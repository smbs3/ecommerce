import { AxiosRestClient } from "@/utils/axiosConfig";
import { createClient } from "@/utils/supabase/client";

const getProducts = async (queryParams: string) => {
  try {
    const supabase = createClient()
    const {data: {user}}  = await supabase.auth.getUser();
    const axiosClient = AxiosRestClient(user?.id);

    const response = await axiosClient.get(`/products`, {
      params: { q: queryParams ?? "" },
    });

    if (response.status === 200) {
      const products = response.data;
      return {
        products: products.products,
      };
    } else {
      throw new Error("Post not found");
    }
  } catch (error) {
    return { products: [] };
  }
};

export default getProducts;