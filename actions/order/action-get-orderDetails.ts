import { AxiosRestClient } from "@/utils/axiosConfig";
import { createClient } from "@/utils/supabase/client";

const getOrderDetails = async ({ orderId }: { orderId: number }) => {
    try {
        const supabase = createClient()
        const { data: { user } } = await supabase.auth.getUser();
        const axiosClient = AxiosRestClient(user?.id);
        const response = await axiosClient.get(`/vieworder/${orderId}`);
        if (response.status === 200) {
            const order = response.data;

            return {
                order: order.order,
                orderDetails:order.orderDetails
            };
        } else {
            throw new Error('cart not found');
        }
    } catch (error) {
        return { order: [] };
    }
};

export default getOrderDetails;