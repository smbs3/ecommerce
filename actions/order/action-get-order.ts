
import { AxiosRestClient } from "@/utils/axiosConfig";
import { createClient } from "@/utils/supabase/server";




const getOrder = async () => {
  
  try {
    const supabase = createClient()
    
    const {data: {user}}  = await supabase.auth.getUser();
    const axiosClient = AxiosRestClient(user?.id);
    const response = await axiosClient.get(`/vieworders`);
    if (response.status === 200) {
      const orders = response.data;
      return {
        orders:orders.orders
      };
    } else {
      throw new Error('cart not found');
    }
  } catch (error) {
    return { orders:[] };
  }
};

export default getOrder;