import { AxiosRestClient } from "@/utils/axiosConfig";
import { createClient } from "@/utils/supabase/client";

type FormState = {
  message: string | any;
}

export async function onFormPostAction(prevState: FormState, data: FormData) {
   const cart_id = data.get('cart_id')
   const total = data.get('total')
   const shipping_address = data.get('shipping_address')

   let message='';

    try {
    const supabase = createClient()

      const {data: {user}}  = await supabase.auth.getUser();
      const axiosClient = AxiosRestClient(user?.id);
      const response = await axiosClient.post(`/orders`, {
        cart_id,
         total,
         shipping_address
      });

      if (response.status === 200) {
        message="success";
      } else {
        throw new Error("Failed to add create order");
      }
    } catch (error) {
      // return { error: 'Failed to add create order' };
    }
  
    return {
      message
   }
   
   
}