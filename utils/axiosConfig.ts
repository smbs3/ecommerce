import axios from 'axios';
import process from 'process';


export const AxiosRestClient = (userId: string = '') => axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_BASE_URL}/api`,
  headers:{
    'Content-Type':'application/json',
    'user-id': userId
  }
});

export const AxioxGraphClient = axios.create({
  baseURL: process.env.REACT_APP_GRAPH_URL,
});
