import { createClient } from "@/utils/supabase/server";
import { NextResponse } from "next/server";

interface Product {
  id: number;
  name: string;
  description: string;
  price: number;
  available: boolean;
  image_url?: string;
}

interface ErrorResponse {
  error: string;
}

export async function GET(request: Request) {
  const supabase = createClient();
  const searchParams = new URL(request.url).searchParams;
  const searchTerm = searchParams.get("q[search]");

  let supabaseQuery = supabase.from("products").select("*");

  if (searchTerm) {
    supabaseQuery = supabaseQuery.ilike("name", `%${searchTerm}%`);
  }

  const { data: products, error } = await supabaseQuery;

  if (error) {
    throw new Error(error.message);
  }

  return NextResponse.json({ products });
}

