import { createClient } from "@/utils/supabase/server";
import { error } from "console";
import { NextRequest, NextResponse } from 'next/server';

export async function POST(req: NextRequest, res: NextResponse) {
  const supabase = createClient();
  const { productId, quantity } = await req.json();

  try {

    const userId = req.headers.get('user-id')


    const { data: existingCart, error: cartError } = await supabase
      .from('shopping_carts')
      .select("*")
      .eq('user_id', userId)
      .eq('completed',false)
      .single()


    if (!existingCart) {
      const { data: card, error: createCartError } = await supabase
        .from('shopping_carts')
        .insert({ user_id: userId })
        .select('*')
        .single()

      const { data, error: insertError } = await supabase
        .from('cart_details')
        .insert({
          cart_id: card.id,
          product_id: productId,
          quantity
        });

    } else {
      const { data, error: insertError } = await supabase
        .from('cart_details')
        .insert({
          cart_id: existingCart.id,
          product_id: productId,
          quantity
        });
    }


    return NextResponse.json({
      status: 200,
    })

  } catch (error: any) {
    return NextResponse.json({
      status: 500,
      error
    })
  }


}