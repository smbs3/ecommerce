import { NextRequest, NextResponse } from 'next/server';


export async function POST(req: NextRequest, res: NextResponse) {
  if (req.method === 'POST') {
   
    await new Promise(resolve => setTimeout(resolve, 1000));
    
    return NextResponse.json({
      body: JSON.stringify({ success: true, message: 'Pago procesado con éxito' }),
      status: 200,
      headers: { 'Content-Type': 'application/json' }
    });
  } else {
 
    return NextResponse.json({
      body: 'Method Not Allowed',
      status: 405
    });
  }
}