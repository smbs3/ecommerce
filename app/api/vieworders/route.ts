import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";


interface viweOrder {
  id: number,
  user_id: string,
  cart_id: number,
  total: number,
  shipping_address: string,
  payment_method: string,
  order_status: string,
}

interface ErrorResponse {
  error: string;
}
export async function GET(req:NextRequest) {
  const supabase = createClient();
  const userId = req.headers.get('user-id')
  

  if (!userId) {
    return NextResponse.json({ error:'user No found' }, { status: 500 });
  }

  const { data: orders, error } = await supabase.from('orders').select('*').eq('user_id', userId);

  if (error) {
    return NextResponse.json({ error: error.message }, { status: 500 });
  }

  return NextResponse.json({ orders });

}

