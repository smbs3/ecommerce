import { createClient } from "@/utils/supabase/client";
import { NextRequest, NextResponse } from 'next/server';

interface newOrder {
  id: number,
  user_id: string,
  cart_id: number,
  total: number,
  shipping_address: string,
  payment_method: string,
  order_status: string,

}


export async function POST(req: NextRequest, res: NextResponse) {
  

  const {cart_id, total, shipping_address } = await req.json();
  const supabase = createClient();
  const user_id = req.headers.get('user-id')
  try {

    const { data, error: cartError } = await supabase
      .from('shopping_carts')
      .update({ completed: 'true' })
      .eq('id', cart_id)
      .select()

    if (cartError) {
      return NextResponse.json({ error: 'Error al actualizar el carrito' });
    }


    const { data: newOrder, error: orderError } = await supabase
      .from('orders')
      .insert([
        {
          user_id,
          cart_id,
          total,
          shipping_address,
          payment_method:'card',
          order_status: 'pending' // Puedes establecer el estado de la orden como pendiente inicialmente
        }
      ]).select();

    if (orderError) {
      throw orderError;
    }
    const order:newOrder[]=newOrder??[]

    if (order){
      return NextResponse.json({ success: true, orderId: order[0].id });
    }

  } catch (error: any) {
    console.error(error.message);
    return NextResponse.json({ error: 'Error processing payment' });
  }

}
