import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";


export async function GET(req:NextRequest, ctx:any) {
  const supabase = createClient();

try {
  const {id}=ctx.params

  const { data:orderDetails, error } = await supabase.from('orders').select().eq('id', id).single();

  const { data: cartDetails, error: detailsError } = await supabase
  .from('cart_details')
  .select(`
    *,
    products:product_id(id,name,description,image_url,price)
  `)
  .eq('cart_id', orderDetails.cart_id)

  const groupedCart = new Map<number, any>();
  cartDetails?.forEach((item: any) => {
    const productId = item.products.id;
    if (groupedCart.has(productId)) {

      const existingItem = groupedCart.get(productId);
      existingItem.quantity += item.quantity;
    } else {

      groupedCart.set(productId, { ...item });
    }
  });
  const mergedCart = Array.from(groupedCart.values());

  return NextResponse.json({
    order: mergedCart,
    orderDetails
  });

} catch (error) {
      return NextResponse.json({ error: 'Error fetching ' });

}




    

  


  

}

