

import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from 'next/server';

interface cartDetails {
  id: number,
  user_id: string,
  completed: boolean
}


export async function GET(req: NextRequest, res: NextResponse) {
  const supabase = createClient();
  const userId = req.headers.get('user-id')


  try {
   

    if (!userId) {
      return NextResponse.json({ error: 'Authentication required' });
    }

    const { data: existingCart, error: cartError } = await supabase
      .from('shopping_carts')
      .select("*")
      .eq('user_id', userId)
      .eq('completed', "FALSE");


    const cartExisting: cartDetails[] = existingCart ?? []

    if (!cartExisting) {
      return NextResponse.json({ error: 'Cart not found' });
    }

    const { data: cartDetails, error: detailsError } = await supabase
      .from('cart_details')
      .select(`
        *,
        products:product_id(id,name,description,image_url,price)
      `)
      .eq('cart_id', cartExisting[0].id)

    if (detailsError) {
      throw detailsError;
    }


    const groupedCart = new Map<number, any>();
    cartDetails.forEach((item: any) => {
      const productId = item.products.id;
      if (groupedCart.has(productId)) {

        const existingItem = groupedCart.get(productId);
        existingItem.quantity += item.quantity;
      } else {

        groupedCart.set(productId, { ...item });
      }
    });

  
    const mergedCart = Array.from(groupedCart.values());

    return NextResponse.json({
      cart: mergedCart,
    });

  } catch (error: any) {
    return NextResponse.json({ error: 'Error fetching cart details' });
  }
}
