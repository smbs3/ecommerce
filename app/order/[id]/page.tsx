import getOrderDetails from '@/actions/order/action-get-orderDetails'
import AuthButton from '@/components/AuthButton'
import DeployButton from '@/components/DeployButton'
import CardOrder from '@/components/cardOrder'
import OrderProduct from '@/components/orderProduct'
import React from 'react'

const page = async ({ params }: { params: { id: number } }) => {

  const { id } = params

  const { order, orderDetails } = await getOrderDetails({ orderId: id })



  return (
   
    <section className="py-24 relative bg-white">
    
      <div className="w-full max-w-7xl px-4 md:px-5 lg-6 mx-auto">
        <h2 className="font-manrope font-bold text-4xl leading-10 text-black text-center">
          Payment Successful
        </h2>
        <p className="mt-4 font-normal text-lg leading-8 text-gray-500 mb-11 text-center">Thanks for making a purchase
          you can
          check our order summary frm below</p>
        <div className="main-box border border-gray-200 rounded-xl pt-6 max-w-xl max-lg:mx-auto lg:max-w-full">
          <div
            className="flex flex-col lg:flex-row lg:items-center justify-between px-6 pb-6 border-b border-gray-200">
            <div className="data">
              <p className="font-semibold text-base leading-7 text-black">Order Id: <span className="text-indigo-600 font-medium">#{id}</span></p>
              <p className="font-semibold text-base leading-7 text-black mt-4">Order Payment : <span className="text-gray-400 font-medium"> {orderDetails.order_date}</span></p>
            </div>

          </div>
          {order?.map((item: any) => <OrderProduct {...item} />)}


          <div className="w-full border-t border-gray-200 px-6 flex flex-col lg:flex-row items-center justify-between ">
            <div className="flex flex-col sm:flex-row items-center max-lg:border-b border-gray-200">
              <p className="font-medium text-lg text-gray-900 pl-6 py-3 max-lg:text-center">Paid using Credit Card <span className="text-gray-500">ending with 8822</span></p>
            </div>
            <p className="font-semibold text-lg text-black py-6">Total Price: <span className="text-indigo-600"> ${orderDetails.total}</span></p>
          </div>

        </div>
      </div>
    </section>
  )
}

export default page