import getOrder from '@/actions/order/action-get-order'
import CardOrder from '@/components/cardOrder';


interface detailsOrder {
  id: number,
  user_id: string,
  total: number,
  order_status: string
}

const viewOrderpage = async () => {
  const {orders} = await getOrder();

  return (
<div className="relative overflow-x-auto">
    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" className="px-6 py-3">
                    # Order
                </th>
                <th scope="col" className="px-6 py-3">
                    Total
                </th>
                <th scope="col" className="px-6 py-3">
                    Status
                </th>
                <th scope="col" className="px-6 py-3">
                    View Order
                </th>
            </tr>
        </thead>
        <tbody>
        {orders?.map((order: detailsOrder) => (
      <CardOrder {...order} />
    ))}
            
        </tbody>
    </table>
</div>
  
  )
}

export default viewOrderpage






