
import getCarts from '@/actions/Cart/action-getCart';
import AuthButton from '@/components/AuthButton';
import CartItem from '@/components/cartItem';
import Link from 'next/link';



interface Product {
  name: string;
  image_url: string;
  price: number;
  description: string;
}
interface cartDetails {
  "id": number,
  "cart_id": number,
  "product_id": number,
  "quantity": number,
  "products": Product
}


const Cart = async () => {

  
  const { cart } = await getCarts()

  if (!cart){
    return ("No existe carrito")
  }
  const total = cart.reduce((acc:any, item:cartDetails) => acc + item.products.price * item.quantity, 0);


  return (
    <div className="container mx-auto px-4">
      <nav className="w-full flex justify-center border-b border-b-foreground/10 h-16">
        <div className="w-full max-w-4xl flex justify-between items-center p-3 text-sm">
          <AuthButton />
        </div>

      </nav>
      <h1 className="text-2xl font-semibold mb-4">Shopping Cart</h1>
      <div className="mx-auto max-w-5xl justify-center px-6 md:flex md:space-x-6 xl:px-0">
        <div className="rounded-lg md:w-2/3">
        {cart?.map((item: cartDetails) => (
            <CartItem key={item.id} {...item} />
          ))}
        </div>

        <div className="w-1/3  p-4 shadow-md">
          <h2 className="text-xl font-bold mb-6">Resumen del pedido</h2>
          <ul>
            {cart.map((items:any) => (
              <li key={`summary-${items.id}`} className="mb-4">
                <div className="flex justify-between">
                  <span>{items.quantity} x {items.products.name}</span>
                  <span>${items.products.price * items.quantity}</span>
                </div>
              </li>
            ))}
          </ul>
          <div className="mt-4 text-lg font-semibold">
            Total a pagar: <span className="font-bold">${total}</span>
          </div>
          <Link href="/checkout" className="mt-8 w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Proceder al pago
          </Link>
        </div>
      </div >
    </div>
  );
};

export default Cart;