import getCarts from '@/actions/Cart/action-getCart';
import Pay from '@/components/pay';


interface Product {
  name: string;
  image_url: string;
  price: number;
  description: string;
}

interface CartDetails {
  id: number;
  cart_id: number;
  product_id: number;
  quantity: number;
  products: Product;
}

const CheckoutPage = async () => {
  const { cart } = await getCarts()
 
  if (!cart){
    return ("No existe carrito")
  }

  const total = cart.reduce((acc: any, item: any) => acc + item.products.price * item.quantity, 0);
  

  return (
    <div className="container mx-auto px-4">
      <h1 className="text-2xl font-semibold mb-4">Confirmación de la compra</h1>
      <ul>
        {cart.map((item: CartDetails) => (
          <li key={`checkout-item-${item.id}`} className="mb-4">
            <div className="flex justify-between">
              <span>{item.quantity} x {item.products.name}</span>
              <span>${item.products.price * item.quantity}</span>
            </div>
          </li>
        ))}
      </ul>
      <div className="text-lg font-semibold">
        Total: <span className="font-bold">${total}</span>
      </div>

      <div>
        <Pay total={total}  cardId={cart[0].cart_id}/>
      </div>
    </div>
  );
}

export default CheckoutPage;

