// pages/success.tsx
import { NextPage } from 'next';
import Link from 'next/link';

const SuccessPage: NextPage = () => {
  return (
    <div className="p-8">
      <h1 className="text-2xl font-bold mb-8">Pago completado con éxito</h1>
      <p>Gracias por tu compra.</p>
      <Link href="/">
        <Link href='/' className="text-blue-500">Volver al inicio</Link>
      </Link>
    </div>
  );
};

export default SuccessPage;
